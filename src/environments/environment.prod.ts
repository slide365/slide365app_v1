export const environment = {
  production: true,
  apiEndpoint: 'https://slide365dev.azurewebsites.net/api/',
  googleEndPoint:
    'https://accounts.google.com/o/oauth2/v2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fdrive.metadata.readonly&include_granted_scopes=true&state=state_parameter_passthrough_value&redirect_uri=https%3A%2F%2Fslide365dev.azurewebsites.net%2FLoginSuccess&response_type=token&client_id=236441090664-fe6d9too40i1gj5e7q4h79i1cr9gnp5q.apps.googleusercontent.com',
  linkedInEndPoint:
    'https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=81ueom6j327vur&redirect_uri=https:%2F%2Fslide365dev.azurewebsites.net%2fLinkednLoginSuccess&state=987654321&scope=r_basicprofile,r_emailaddress'
};

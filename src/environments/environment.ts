// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiEndpoint: 'https://slide365dev.azurewebsites.net/api/',
  // apiEndpoint: 'http://localhost:49350/',
  googleEndPoint:
    'https://accounts.google.com/o/oauth2/v2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fdrive.metadata.readonly&include_granted_scopes=true&state=state_parameter_passthrough_value&redirect_uri=http%3A%2F%2Flocalhost%3A4200%2FLoginSuccess&response_type=token&client_id=236441090664-fe6d9too40i1gj5e7q4h79i1cr9gnp5q.apps.googleusercontent.com',
  linkedInEndPoint:
    'https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=81ueom6j327vur&redirect_uri=http:%2F%2Flocalhost:4200%2fLinkednLoginSuccess&state=987654321&scope=r_basicprofile,r_emailaddress'

  /*
  apiEndpoint: 'https://slide365dev.azurewebsites.net/api/',
  googleEndPoint:
    'https://accounts.google.com/o/oauth2/v2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fdrive.metadata.readonly&include_granted_scopes=true&state=state_parameter_passthrough_value&redirect_uri=http%3A%2F%2Flocalhost%3A4200%2FLoginSuccess&response_type=token&client_id=236441090664-fe6d9too40i1gj5e7q4h79i1cr9gnp5q.apps.googleusercontent.com',
  // 'https://accounts.google.com/o/oauth2/v2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fdrive.metadata.readonly&include_granted_scopes=true&state=state_parameter_passthrough_value&redirect_uri=http%3A%2F%2Flocalhost%3A4200%2Fhome&response_type=token&client_id=236441090664-fe6d9too40i1gj5e7q4h79i1cr9gnp5q.apps.googleusercontent.com',
  // 'https://accounts.google.com/o/oauth2/v2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fdrive.metadata.readonly&include_granted_scopes=true&state=state_parameter_passthrough_value&redirect_uri=https%3A%2F%2Fslide365dev.azurewebsites.net%2FLoginSuccess&response_type=token&client_id=236441090664-fe6d9too40i1gj5e7q4h79i1cr9gnp5q.apps.googleusercontent.com',

  linkedInEndPoint:
    'https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=81ueom6j327vur&redirect_uri=http:%2F%2Flocalhost:4200%2fLinkednLoginSuccess&state=987654321&scope=r_basicprofile,r_emailaddress'
    */
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

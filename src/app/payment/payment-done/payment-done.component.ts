import { Component, OnInit } from '@angular/core';
import { OrderHistory } from 'src/app/_shared/models/order-history';
import { ActivatedRoute } from '@angular/router';
import { CoreService } from 'src/app/_core/services/core.service';

@Component({
  selector: 'app-payment-done',
  templateUrl: './payment-done.component.html',
  styleUrls: ['./payment-done.component.scss']
})
export class PaymentDoneComponent implements OnInit {
  orderHistory: OrderHistory[];
  public downloadZipLink;
  constructor(
    private route: ActivatedRoute,
    private coreService: CoreService
  ) {}

  ngOnInit() {
  }

  Download(): void {
    this.coreService.DownloadAndOpenPowerPoint(this.route.snapshot.params.Id).subscribe(
      response => {
        PowerPoint.createPresentation(response);
      },
      error => {
      }
    );
  }

  DownloadFile(blob): void {
    const url = window.URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = url;
    link.download = 'archive.zip';
    link.click();

    window.URL.revokeObjectURL(url);
  }
}

import {
  Component,
  OnInit,
  ElementRef,
  ViewChild,
  OnDestroy,
  AfterViewInit,
  ChangeDetectorRef
} from '@angular/core';
import { ShowCart } from '../_shared/models/ShowCart';
import { Router, ActivatedRoute } from '@angular/router';
import { CoreService } from '../_core/services/core.service';
import { NgForm } from '@angular/forms';
import { ToastrManager } from 'ng6-toastr-notifications';
declare var stripe: any;
declare var elements: any;
@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('cardInfo') cardInfo: ElementRef;
  showcart: ShowCart[];
  public CartAmount;
  card: any;
  cardHandler = this.onChange.bind(this);
  error: string;

  constructor(
    private cd: ChangeDetectorRef,
    private coreService: CoreService,
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrManager
  ) {}

  ngOnInit() {
    this.Close();
    this.GetCart();
  }

  Close(): void {}

  ngAfterViewInit() {
    const style = {
      base: {
        lineHeight: '24px',
        fontFamily: 'monospace',
        fontSmoothing: 'antialiased',
        fontSize: '19px',
        '::placeholder': {
          color: 'purple'
        }
      }
    };
    setTimeout(() => {
      if (elements ? true : false) {
        this.card = elements.create('card', { style });
        this.card.mount(this.cardInfo.nativeElement);

        this.card.addEventListener('change', this.cardHandler);
        document.getElementsByTagName('iframe')[0].style.width = 'auto';
      }
    }, 2000);
  }

  ngOnDestroy() {
    if (this.card) {
      this.card.removeEventListener('change', this.cardHandler);
      this.card.destroy();
    }
  }

  onChange({ error }) {
    if (error) {
      this.error = error.message;
    } else {
      this.error = null;
    }
    this.cd.detectChanges();
  }

  async onSubmit(form: NgForm) {

    const { token, error } = await stripe.createToken(this.card);
    if (error) {
    } else {
      this.coreService.GetPayment(token.id).subscribe(order => {
        this.router.navigate(['payment/payment-done/' + order]);
      });
    }
  }

  GetCart(): void {
    // const userid = sessionStorage.getItem('User');
    const userid = sessionStorage.getItem('userInfo');

    if (userid != null) {
      this.coreService.ShowtoCart().subscribe((_showcart: any) => {
        this.showcart = _showcart;
        this.CartAmount = 0;

        if (this.showcart != null) {
          for (let i = 0; i < this.showcart.length; i++) {
            this.CartAmount += this.showcart[i].Price;
          }
        }
      });
    } else {
      this.toastr.warningToastr('Please login to continue...', 'Alert!', {
        position: 'top-center',
        enableHTML: true
      });
      this.router.navigate(['home']);
    }
  }
}

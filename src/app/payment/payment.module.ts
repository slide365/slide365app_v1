import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaymentRoutingModule } from './payment-routing.module';
import { PaymentComponent } from './payment.component';
import { FormsModule } from '@angular/forms';
import { PaymentDoneComponent } from './payment-done/payment-done.component';

@NgModule({
  declarations: [PaymentComponent, PaymentDoneComponent],
  imports: [CommonModule, PaymentRoutingModule, FormsModule]
})
export class PaymentModule {}

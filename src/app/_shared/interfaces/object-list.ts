export interface ObjectList {
  ObjectIid: string;
  ObjectName: string;
  ObjectType: string;
  Price: number;
  ObjectimagePath: string;
  ObjectFilePath: string;
  ObjectTag: string;
  CreatedDate: Date | string;
  CreatedById: string;
  ModifiedById: string;
  ModifiedDate: Date | string;
  IsActive: boolean;
}

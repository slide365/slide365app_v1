export interface Domains {
  DomainId: string;
  DomainName: string;
  DomainImagePath: string;
  CreatedDate: Date | string;
  CreatedById: string;
  ModifiedById: string;
  IsActive: boolean;
  DomainKeyword: string;
}

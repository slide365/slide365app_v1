import { ObjectList } from './object-list';

export interface ObjectListing {
  Count: number;
  ObjectArray: ObjectList[];
}

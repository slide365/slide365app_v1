export interface Category {
  CategoryId: string;
  CategoryName: string;
  CategoryImagePath: string;
  CreatedDate: Date | string;
  CreatedById: string;
  ModifiedById: string;
  IsActive: boolean;
  CategoryDescription: string;
  CategoryKeyword: string;
}

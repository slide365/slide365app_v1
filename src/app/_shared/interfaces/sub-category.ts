export interface SubCategory {
  SubCategoryUId: string;
  CategoryName: string;
  SubCategoryName: string;
  CreatedDate: Date | string;
  CreatedByUId: string;
  ModifiedByUId: string;
  IsActive: boolean;
  SubCategoryDescription: string;
  SubCategoryImagePath: string;
}

export interface User {
  UserID: string;
  Name: string;
  EmailID: string;
  AccessCode: string;
  Expires_in: string;
  provider: string;
  IsAcive: boolean;
  ProfilePic: string;
  SessionId: string;
  lastLogin: Date | string;
}

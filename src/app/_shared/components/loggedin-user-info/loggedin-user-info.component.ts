import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import * as moment from 'moment';
import { OrderHistory } from '../../models/order-history';
import { CoreService } from 'src/app/_core/services/core.service';
import { UserInfo } from '../../models/user-info';

@Component({
  selector: 'app-loggedin-user-info',
  templateUrl: './loggedin-user-info.component.html',
  styleUrls: ['./loggedin-user-info.component.scss']
})
export class LoggedinUserInfoComponent implements OnInit {
  public DateFrom;
  public DateTo;
  public TotalOrder;
  orderHistory: OrderHistory[];
  public user;
  userInfo = new UserInfo();
  @Output() eventEmitter: EventEmitter<any> = new EventEmitter();

  constructor(private coreService: CoreService) {}

  ngOnInit() {
    // this.user = JSON.parse(sessionStorage.getItem('User'));
    this.user = JSON.parse(sessionStorage.getItem('userInfo'));
    this.userInfo = JSON.parse(sessionStorage.getItem('userInfo'));
    this.TotalOrder = 0;
    this.DateFrom = new Date();
    this.DateFrom.setHours(0, 0, 0, 0);
    this.DateTo = new Date();
    this.DateTo.setHours(23, 59, 59, 999);

    this.DateFrom = moment(this.DateFrom).format('LLL');
    this.DateTo = moment(this.DateTo).format('LLL');
    this.eventEmitter.emit({ isShowSpinner: true });

    this.coreService
      .GetOrderHistory(this.DateFrom, this.DateTo, 1, 10)
      .subscribe((_OrderHistory: any) => {
        this.eventEmitter.emit({ isShowSpinner: false });

        this.orderHistory = _OrderHistory;
        if (this.orderHistory != null) {
          this.TotalOrder = this.orderHistory.length;
        }
      });
  }

  Logout(): void {
    this.eventEmitter.emit({ isShowSpinner: true });
    sessionStorage.clear();
    this.coreService.TotalItem = 0;
    this.eventEmitter.emit({ loginModal: false });
    this.eventEmitter.emit({ isShowSpinner: false });
  }
}

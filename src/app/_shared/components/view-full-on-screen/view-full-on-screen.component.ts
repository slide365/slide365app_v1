import {
  Component,
  OnInit,
  Input,
  Output,
  OnChanges,
  EventEmitter
} from '@angular/core';
import { CoreService } from 'src/app/_core/services/core.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-view-full-on-screen',
  templateUrl: './view-full-on-screen.component.html',
  styleUrls: ['./view-full-on-screen.component.scss']
})
export class ViewFullOnScreenComponent implements OnInit, OnChanges {
  @Input() imageDetails: any;
  @Output() clearQueue: EventEmitter<any> = new EventEmitter();
  constructor(private coreService: CoreService, public toastr: ToastrManager) {}

  ngOnInit() {}

  ngOnChanges() {}

  closeModal() {
    let classObj = document.getElementById('image-on-full-screen');
    classObj.classList.remove('fadeInLeft');
    classObj.classList.add('fadeOutRight');

    setTimeout(() => {
      classObj = document.getElementById('image-on-full-screen');
      classObj.classList.remove('fadeOutRight');
      classObj.classList.add('fadeInLeft');
      this.clearQueue.emit();
    }, 1000);
  }

  AddtoCart(ObjectID: string): void {
    // const userid = sessionStorage.getItem('User');
    const userid = sessionStorage.getItem('userInfo');

    if (userid != null) {
      this.coreService.AddtoCart(ObjectID).subscribe(
        () => {
          this.coreService.callAddToCartService();
          this.closeModal();
        },
        error =>
          this.toastr.errorToastr(error.error.Message, null, {
            position: 'top-center',
            enableHTML: true
          })
      );
    } else {
      this.toastr.warningToastr('Please login to continue...', null, {
        position: 'top-center',
        enableHTML: true
      });
    }
  }
}

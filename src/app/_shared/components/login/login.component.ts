import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter
} from '@angular/core';
import { environment } from '../../../../environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { CoreService } from 'src/app/_core/services/core.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnChanges {
  show = false;
  @Input() showModel: boolean;
  @Output() eventEmitter: EventEmitter<any> = new EventEmitter();
  public IsLogin;
  public newwindow;
  public newgooglewin;
  public newmicrosoftwin;
  public uuid;
  public Tnc = false;

  constructor(
    public spinner: NgxSpinnerService,
    public coreService: CoreService,
  ) {
    this.coreService.closeLoginModal.subscribe((value: any) => {
      if (value) {
        this.IsLogin =
          sessionStorage.getItem('userInfo') == null ? false : true;
        this.eventEmitter.emit({ isLoggedInUser: this.IsLogin });
        this.Close();
      }
    });
  }

  ngOnInit() {
    this.Tnc = false;
    // this.uuid = sessionStorage.getItem('User');
    this.uuid = sessionStorage.getItem('userInfo');
    this.IsLogin = this.uuid == null ? false : true;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes
        ? changes.showModel
          ? changes.showModel.currentValue === true
          : false
        : false
    ) {
      this.show = !this.show;
      // this.uuid = sessionStorage.getItem('User');
      this.uuid = sessionStorage.getItem('userInfo');
      this.IsLogin = this.uuid == null ? false : true;
      this.eventEmitter.emit({ isLoggedInUser: this.IsLogin });
    }
  }

  Close(flag = 0): void {
    let classObj = document.getElementById('userLoginModal');
    classObj.classList.remove('fadeInLeft');
    classObj.classList.add('fadeOutRight');

    setTimeout(() => {
      classObj = document.getElementById('userLoginModal');
      classObj.classList.remove('fadeOutRight');
      classObj.classList.add('fadeInLeft');
      this.show = false;
      if (flag) {
        this.ngOnInit();
      }
    }, 1000);
  }

  NewPopup(): boolean {
    if (sessionStorage.getItem('UserID') == null) {
      this.CreateGuid();
    }
    Object.defineProperty(window, 'getUser', {
      value: function(user) {
        sessionStorage.setItem('User', JSON.stringify(user));
        sessionStorage.removeItem('UserID');
        this.IsLogin = true;
      }
    });
    sessionStorage.setItem('LastPage', window.location.href);
    window.location.href = environment.linkedInEndPoint;

    return false;
  }

  GoogleLogin(): boolean {
    this.spinner.show();
    if (sessionStorage.getItem('UserID') == null) {
      this.CreateGuid();
    }
    Object.defineProperty(window, 'getUser', {
      value: function(user) {
        sessionStorage.setItem('User', JSON.stringify(user));
        sessionStorage.removeItem('UserID');
        this.IsLogin = true;
      }
    });
    sessionStorage.setItem('LastPage', window.location.href);
    window.location.href = environment.googleEndPoint;
    this.spinner.hide();
    return false;
  }

  MicrpSoftLogin(): boolean {
    if (sessionStorage.getItem('UserID') == null) {
      this.CreateGuid();
    }
    this.newmicrosoftwin = window.open(
      'https://login.microsoftonline.com/common/oauth2/authorize?client_id=a21ea4ca-96a4-401c-9907-a6db969d43b4&response_type=code&redirect_uri=http%3A%2F%2Flocalhost%3A4200%2FMSSuccess&response_mode=query&resource=http://localhost:4200/ccf04988-4b14-447e-90db-999c0b64f571&state=12345'
    );

    if (window.focus) {
      this.newmicrosoftwin.focus();
    }
    return false;
  }

  CreateGuid(): void {
    const s = [];
    const hexDigits = '0123456789abcdef';
    for (let i = 0; i < 36; i++) {
      s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = '4';
    // tslint:disable-next-line:no-bitwise
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
    s[8] = s[13] = s[18] = s[23] = '-';

    this.uuid = s.join('');
    sessionStorage.setItem('UserID', this.uuid);
  }

  callFromChildComponent(evt) {
    const keyname = Object.keys(evt);
    switch (keyname[0]) {
      case 'loginModal': {
        // tslint:disable-next-line:no-unused-expression
        !evt[keyname[0]] ? this.Close(1) : null;
        // this.uuid = sessionStorage.getItem('User');
        this.uuid = sessionStorage.getItem('userInfo');
        this.IsLogin = this.uuid == null ? false : true;
        this.eventEmitter.emit({ isLoggedInUser: this.IsLogin });
        break;
      }

      case 'isShowSpinner': {
        evt[keyname[0]] ? this.spinner.show() : this.spinner.hide();
        break;
      }
    }
  }

  microsoftLogin() {
    this.coreService.msLogin();
  }

  /**
   * This function is used to login with google
   */
  onClickOfGoogleLogin() {
    // this.authService.login();
  }
}

import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { CoreService } from 'src/app/_core/services/core.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnChanges {
  show = false;
  @Input() showSettings: boolean;
  public Name;
  public Email;
  public Subject;
  public Message;
  public Problem;
  public Comments;

  constructor(private coreService: CoreService) {}

  ngOnInit() {
    setTimeout(() => {
      const btn = document.getElementById('ngb-panel-0-header');
      btn.addEventListener('click', () => {
        this.show = false;
        location.reload();
      });
    }, 1000);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes
        ? changes.showSettings
          ? changes.showSettings.currentValue === true
          : false
        : false
    ) {
      this.show = !this.show;
    }
  }

  Close(flag = 0): void {
    let classObj = document.getElementById('settingUserModal');
    classObj.classList.remove('fadeInLeft');
    classObj.classList.add('fadeOutRight');

    setTimeout(() => {
      classObj = document.getElementById('settingUserModal');
      classObj.classList.remove('fadeOutRight');
      classObj.classList.add('fadeInLeft');
      this.show = false;
      if (flag) {
        this.ngOnInit();
      }
    }, 1000);
  }

  ContactUs(): void {
    if (this.Name == null || this.Name === '') {
      return alert('Name is Required');
    }
    if (this.Email == null || this.Email === '') {
      return alert('Email is Required');
    }
    if (this.Subject == null || this.Subject === '') {
      return alert('Subject is Required');
    }
    if (this.Message == null || this.Message === '') {
      return alert('Message is Required');
    }
    this.coreService
      .ContactUS(this.Name, this.Email, this.Subject, this.Message)
      .subscribe(null, error => alert(error.error.Message));
  }

  Report(): void {
    if (this.Problem == null || this.Problem === '') {
      return alert('Problem is Required');
    }
    if (this.Comments == null || this.Comments === '') {
      return alert('Comments is Required');
    }
    this.coreService
      .ReportProblem(this.Problem, this.Comments)
      .subscribe(null, error => alert(error.error.Message));
  }

  reload() {
    location.reload();
  }
}

import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onClickOfItem: EventEmitter<any> = new EventEmitter();

  @Input() dataCollection = [];
  @Input() propertyName: string;
  @Input() sliderName: string;
  @Input() showNavigationIndicators = false;

  constructor() {}

  ngOnInit() {}

  onClickOfImage(item) {
    this.onClickOfItem.emit({ item: item });
  }
}

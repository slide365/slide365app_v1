export class Constants {
  public static clientIdForMicrosoftLogin =
    '7a4ab922-2322-424b-a305-4aa3e85b478b';
  // '85f1bc0f-5f24-4a12-a55e-159aa5e5ca1b';

  // For deveopment purpose we kept the domains here
  /* Production URL */
  apiEndpoint = 'https://slide365dev.azurewebsites.net/api/';
  // googleEndPoint = "https://accounts.google.com/o/oauth2/v2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fdrive.metadata.readonly&include_granted_scopes=true&state=state_parameter_passthrough_value&redirect_uri=https%3A%2F%2Fslide365dev.azurewebsites.net%2FLoginSuccess&response_type=token&client_id=236441090664-fe6d9too40i1gj5e7q4h79i1cr9gnp5q.apps.googleusercontent.com";
  // linkedInEndPoint = "https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=81ueom6j327vur&redirect_uri=https:%2F%2Fslide365dev.azurewebsites.net%2fLinkednLoginSuccess&state=987654321&scope=r_basicprofile,r_emailaddress";

  /* Local URL */
  // apiEndpoint = 'http://localhost:49350/';
  googleEndPoint =
    'https://accounts.google.com/o/oauth2/v2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fdrive.metadata.readonly&include_granted_scopes=true&state=state_parameter_passthrough_value&redirect_uri=http%3A%2F%2Flocalhost%3A4200%2FLoginSuccess&response_type=token&client_id=236441090664-fe6d9too40i1gj5e7q4h79i1cr9gnp5q.apps.googleusercontent.com';
  linkedInEndPoint =
    'https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=81ueom6j327vur&redirect_uri=http:%2F%2Flocalhost:4200%2fLinkednLoginSuccess&state=987654321&scope=r_basicprofile,r_emailaddress';
}

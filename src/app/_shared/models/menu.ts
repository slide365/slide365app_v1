import { Menulist } from './menulist';

export class Menu {
  Name: string;
  MenuList: Menulist[];
  Type: string;
}

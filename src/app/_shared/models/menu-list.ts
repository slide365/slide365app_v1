export class MenuList {
  Name: string;
  Type: string;
  MenuList: any[];

  constructor() {
    this.Name = 'Main Menu';
    this.Type = 'Main';
    this.MenuList = [
      {
        Id: '1',
        Name: 'Template'
      },
      {
        Id: '2',
        Name: 'Domain'
      },
      {
        Id: '3',
        Name: 'Design'
      },
      {
        Id: '4',
        Name: 'Icons'
      }
    ];
  }
}

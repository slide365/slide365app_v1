export class UserInfo {
  UserID: string;
  Name: string;
  EmailID: string;
  AccessCode: string;
  Expires_in: string;
  provider: string;
  IsAcive: boolean;
  ProfilePic: string;
  SessionId: string;

  constructor() {
    this.UserID = null;
    this.Name = null;
    this.EmailID = null;
    this.AccessCode = null;
    this.Expires_in = '3600';
    this.provider = null;
    this.IsAcive = true;
    this.ProfilePic = null;
    this.SessionId = null;
  }
}

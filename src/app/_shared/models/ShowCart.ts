export class ShowCart {
  UserCartId: string;
  UserId: string;
  AddedDate: Date;
  ObjectIid: string;
  ObjectImagePath: string;
  Price: number;
  ObjectName: string;
}

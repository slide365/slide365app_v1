export class OrderHistory {
  ObjectIid: string;
  ObjectImagePath: string;
  ObjectName: string;
  ObjectFilePath: string;
  Price: number;
  PurchaseDate: Date;
  Amount: number;
  DownloadDate: Date;
}

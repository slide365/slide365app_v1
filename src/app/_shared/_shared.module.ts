import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SliderComponent } from './components/slider/slider.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from './components/login/login.component';
import { FormsModule } from '@angular/forms';
import { LoggedinUserInfoComponent } from './components/loggedin-user-info/loggedin-user-info.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ViewFullOnScreenComponent } from './components/view-full-on-screen/view-full-on-screen.component';
import { CartComponent } from './components/cart/cart.component';
import { SettingsComponent } from './components/settings/settings.component';

@NgModule({
  declarations: [
    SliderComponent,
    LoginComponent,
    LoggedinUserInfoComponent,
    ViewFullOnScreenComponent,
    CartComponent,
    SettingsComponent
  ],
  imports: [CommonModule, NgbModule, FormsModule, NgxSpinnerModule],
  exports: [
    SliderComponent,
    LoginComponent,
    ViewFullOnScreenComponent,
    CartComponent,
    SettingsComponent,
    LoggedinUserInfoComponent
  ],
  providers: []
})
export class SharedModule {}

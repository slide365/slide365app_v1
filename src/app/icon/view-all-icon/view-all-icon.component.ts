import { Component, OnInit } from '@angular/core';
import { CoreService } from 'src/app/_core/services/core.service';
import { Domains } from 'src/app/_shared/interfaces/domains';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-all-icon',
  templateUrl: './view-all-icon.component.html',
  styleUrls: ['./view-all-icon.component.scss']
})
export class ViewAllIconComponent implements OnInit {
  Icons: Domains[];

  constructor(
    public coreService: CoreService,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {}

  ngOnInit() {
    this.GetIcons();
  }

  GoToCategory(DomainID: string, DomainName: string): void {
    sessionStorage.setItem('isAll', 'True');
    sessionStorage.setItem('IconName', DomainName);
    sessionStorage.setItem('IconID', DomainID);
    this.router.navigate(['icon/specific-icons/' + DomainID]);
  }

  GetIcons(): void {
    this.spinner.show();
    this.coreService.getAllIcons().subscribe(domains => {
      this.Icons = domains;
      this.spinner.hide();
    });
  }
}

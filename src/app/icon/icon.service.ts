import { Injectable } from '@angular/core';
import { DataService } from '../_core/services/data.service';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class IconService {
  constructor(private dataService: DataService) {}

  // GetObjectList(
  //   DomainID: string,
  //   SubCategoryID: string,
  //   Pageno: number,
  //   PageSize: number
  // ) {
  //   return this.dataService.getData(
  //     'api/Category/GetIconsByIconUid?IconUid=' +
  //       DomainID +
  //       '&SubCategoryId=' +
  //       SubCategoryID +
  //       '&Pageno=' +
  //       Pageno +
  //       '&PageSize=' +
  //       PageSize
  //   );
  // }

  GetObjectList(DomainID: string, Pageno: number, PageSize: number) {
    return this.dataService
      .getData(
        'api/Category/GetObjectList?FileObjectUID=' +
          DomainID +
          '&Pageno=' +
          Pageno +
          '&PageSize=' +
          PageSize
      )
      .pipe(map(res => res));
  }
}

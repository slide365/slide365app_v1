import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewAllIconComponent } from './view-all-icon/view-all-icon.component';
import { SpecificIconsComponent } from './specific-icons/specific-icons.component';

const routes: Routes = [
  {
    path: 'view-all',
    component: ViewAllIconComponent,
    data: {
      title: 'view-all'
    }
  },
  {
    path: 'specific-icons/:Id',
    component: SpecificIconsComponent,
    data: {
      title: 'view-all'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IconRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { CoreService } from 'src/app/_core/services/core.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { IconService } from '../icon.service';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-specific-icons',
  templateUrl: './specific-icons.component.html',
  styleUrls: ['./specific-icons.component.scss']
})
export class SpecificIconsComponent implements OnInit {
  IsAll = false;
  DomainName: string;
  ObjectListing: any;
  PageNo: number;
  PageSize: number;
  TotalPage: number;
  public IsSideBar;
  imageDetails = null;
  ngxLoading = true;

  constructor(
    private iconService: IconService,
    public coreService: CoreService,
    public toastr: ToastrManager,
    private spinner: NgxSpinnerService
  ) {
    this.ObjectListing = {
      Count: 0,
      ObjectArray: []
    };
  }

  ngOnInit() {
    this.IsAll = sessionStorage.getItem('isAll') === 'False' ? false : true;
    this.DomainName = sessionStorage.getItem('IconName');
    this.PageNo = 1;
    this.PageSize = 8;
    this.GetObject(this.PageNo);
  }

  SideBar(): void {
    this.IsSideBar = true;
  }

  GetObject(PageNo: number): void {
    const DomainID = sessionStorage.getItem('IconID');
    this.spinner.show();

    this.iconService.GetObjectList(DomainID, PageNo, this.PageSize).subscribe(
      _ObjectListing => {
        this.ngxLoading = false;
        this.spinner.hide();
        this.ObjectListing = {
          Count: _ObjectListing.Count,
          ObjectArray: _ObjectListing.ObjectArray
        };

        this.TotalPage = Math.ceil(this.ObjectListing.Count / this.PageSize);
      },
      () => {
        this.ngxLoading = false;
        this.spinner.hide();
      }
    );
  }

  FirstPage(): void {
    if (this.PageNo !== 1) {
      this.PageNo = 1;
      this.GetObject(1);
    }
  }

  NextPage(): void {
    if (this.PageNo !== this.TotalPage) {
      this.PageNo++;
      this.GetObject(this.PageNo);
    }
  }
  PreviousPage(): void {
    if (this.PageNo !== 1) {
      this.PageNo--;
      this.GetObject(this.PageNo);
    }
  }

  LastPage(): void {
    if (this.PageNo !== this.TotalPage) {
      this.PageNo = this.TotalPage;
      this.GetObject(this.TotalPage);
    }
  }

  AddtoCart(ObjectID: string): void {
    // const userid = sessionStorage.getItem('User');
    const userid = sessionStorage.getItem('userInfo');

    if (userid != null) {
      this.coreService.AddtoCart(ObjectID).subscribe(
        () => {
          this.coreService.callAddToCartService();
        },
        error =>
          this.toastr.errorToastr(error.error.Message, null, {
            position: 'top-center',
            enableHTML: true
          })
      );
    } else {
      this.toastr.warningToastr('Please login to continue...', null, {
        position: 'top-center',
        enableHTML: true
      });
    }
  }

  openPopup(msg: string): void {}

  Download(ObjectIid: string): void {
    try {
      const text = (<any>window).external.InsertIcon(ObjectIid);
    } catch (error) {}
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IconRoutingModule } from './icon-routing.module';
import { ViewAllIconComponent } from './view-all-icon/view-all-icon.component';
import { IconService } from './icon.service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { SpecificIconsComponent } from './specific-icons/specific-icons.component';
import { SharedModule } from '../_shared/_shared.module';

@NgModule({
  declarations: [ViewAllIconComponent, SpecificIconsComponent],
  imports: [CommonModule, IconRoutingModule, NgxSpinnerModule, SharedModule],
  providers: [IconService]
})
export class IconModule {}

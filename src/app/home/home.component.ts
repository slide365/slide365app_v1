import { Component, OnInit } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { CoreService } from '../_core/services/core.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [NgbCarouselConfig]
})
export class HomeComponent implements OnInit {
  allTemplates = [];
  allDomains = [];
  allDesigns = [];
  allIcons = [];
  constructor(
    private spinner: NgxSpinnerService,
    config: NgbCarouselConfig,
    public coreService: CoreService,
    private router: Router
  ) {
    config.interval = 2000;
    config.wrap = true;
    config.keyboard = false;
    config.pauseOnHover = false;
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.getDefaultData();
  }

  getDefaultData() {
    this.spinner.show();
    this.coreService.getAllTemplates().subscribe(res => {
      this.allTemplates = res;
    });

    this.coreService.getAllDomains().subscribe(res => {
      this.allDomains = res;
    });

    this.coreService.getAllDesign().subscribe(res => {
      this.allDesigns = res;
    });

    this.coreService.getAllIcons().subscribe(res => {
      this.spinner.hide();
      this.allIcons = res;
    });
  }

  onClickOfTemplate(data) {
    sessionStorage.setItem('isAll', 'False');
    sessionStorage.setItem('CategoryID', data.item.CategoryId);
    this.router.navigate([
      '/template/view-all-specific-template/' + data.item.CategoryId
    ]);
  }

  onClickOfDomain(data) {
    sessionStorage.setItem('isAll', 'False');
    sessionStorage.setItem('DomainName', data.item.DomainName);
    sessionStorage.setItem('DomainID', data.item.DomainId);
    sessionStorage.removeItem('backId');
    this.router.navigate(['/domain/specific-domain/' + data.item.DomainId]);
  }

  onClickOfDesign(data) {
    sessionStorage.setItem('isAll', 'False');
    sessionStorage.setItem('DesignName', data.item.DesignName);
    sessionStorage.setItem('DesignID', data.item.DesignUid);
    sessionStorage.removeItem('backId');
    this.router.navigate(['/design/specific-design/' + data.item.DesignUid]);
  }

  onClickOfIcon(data) {
    sessionStorage.setItem('isAll', 'False');
    sessionStorage.setItem('IconName', data.item.IconName);
    sessionStorage.setItem('IconID', data.item.IconUid);
    this.router.navigate(['/icon/specific-icons/' + data.item.IconUid]);
  }
}

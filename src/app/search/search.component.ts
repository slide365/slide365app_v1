import { Component, OnInit } from '@angular/core';
import { ObjectListing } from '../_shared/interfaces/object-listing';
import { ActivatedRoute } from '@angular/router';
import { CoreService } from '../_core/services/core.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  ObjectListing: ObjectListing;
  PageNo: number;
  PageSize: number;
  TotalPage: number;
  public IsSideBar;
  constructor(
    // private templateservice: TemplateService,
    // private ToggleService: ToggleService,
    public coreService: CoreService,
    // private CartAddRemoveService: CartAddRemoveService,
    private route: ActivatedRoute
  ) {
    // this.ObjectListing = {
    //   Count: 0,
    //   ObjectArray: []
    // };
    route.params.subscribe(val => {
      this.ObjectListing = {
        Count: 0,
        ObjectArray: []
      };
      this.PageNo = 1;
      this.PageSize = 8;
      this.GetObject(this.PageNo);
      // this.ToggleService.change.subscribe(IsSideBar => {
      //   this.IsSideBar = IsSideBar;
      //   if (!IsSideBar) {
      //     this.GetObject(this.PageNo);
      //   }
      // });
    });
  }

  ngOnInit() {
    // this.PageNo = 1;
    // this.PageSize = 8;
    // this.GetObject(this.PageNo);
    // this.ToggleService.change.subscribe(IsSideBar => {
    //   this.IsSideBar = IsSideBar;
    //   if (!IsSideBar) {
    //     this.GetObject(this.PageNo);
    //   }
    // });
  }

  SideBar(): void {
    this.IsSideBar = true;
    // this.ToggleService.toggle();
  }

  GetObject(PageNo: number): void {
    this.coreService
      .GeSearchObjectList(
        this.route.snapshot.params.SearchParam,
        PageNo,
        this.PageSize
      )
      .subscribe(_ObjectListing => {
        this.ObjectListing = {
          Count: _ObjectListing.Count,
          ObjectArray: _ObjectListing.ObjectArray
        };
        this.TotalPage = Math.ceil(this.ObjectListing.Count / this.PageSize);
        // this.ObjectListing.Count % this.PageSize === 0
        //   ? Math.round(this.ObjectListing.Count / this.PageSize)
        //   : Math.round(this.ObjectListing.Count / this.PageSize) + 1;
      });
  }

  FirstPage(): void {
    if (this.PageNo !== 1) {
      this.PageNo = 1;
      this.GetObject(1);
    }
  }

  NextPage(): void {
    if (this.PageNo !== this.TotalPage) {
      this.PageNo++;
      this.GetObject(this.PageNo);
    }
  }
  PreviousPage(): void {
    if (this.PageNo !== 1) {
      this.PageNo--;
      this.GetObject(this.PageNo);
    }
  }
  LastPage(): void {
    if (this.PageNo !== this.TotalPage) {
      this.PageNo = this.TotalPage;
      this.GetObject(this.TotalPage);
    }
  }

  AddtoCart(ObjectID: string): void {
    // const userid = sessionStorage.getItem('User');
    const userid = sessionStorage.getItem('userInfo');

    if (userid != null) {
      this.coreService.AddtoCart(ObjectID).subscribe(cart => {
        // this.CartAddRemoveService.toggle();
        this.coreService.callAddToCartService();
      });
    } else {
      alert('Please Log In');
    }
  }
}

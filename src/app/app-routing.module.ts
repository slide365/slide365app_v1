import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MasterLayoutComponent } from './_core/master-layout/master-layout.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: '',
    component: MasterLayoutComponent,
    data: {
      title: 'home'
    },
    children: [
      {
        path: 'home',
        loadChildren: './home/home.module#HomeModule'
      },
      {
        path: 'template',
        loadChildren: './template/template.module#TemplateModule'
      },
      {
        path: 'domain',
        loadChildren: './domain/domain.module#DomainModule'
      },
      {
        path: 'design',
        loadChildren: './design/design.module#DesignModule'
      },
      {
        path: 'icon',
        loadChildren: './icon/icon.module#IconModule'
      },
      {
        path: 'search',
        loadChildren: './search/search.module#SearchModule'
      },
      {
        path: 'LoginSuccess',
        loadChildren: './account/account.module#AccountModule'
      },
      {
        path: 'payment',
        loadChildren: './payment/payment.module#PaymentModule'
      },
      {
        path: 'cart',
        loadChildren: './cart/cart.module#CartModule'
      },
      {
        path: 'subscription',
        loadChildren: './subscription/subscription.module#SubscriptionModule'
      },
      {
        path: 'auth',
        loadChildren: './auth/auth.module#AuthModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}

import { Component } from '@angular/core';
import { ConnectionService } from 'ng-connection-service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { CoreService } from './_core/services/core.service';
import { UserInfo } from './_shared/models/user-info';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isConnected = true;
  userInfo = new UserInfo();
  constructor(
    private connectionService: ConnectionService,
    public toastr: ToastrManager,
    private coreService: CoreService,
  ) {
    // To check internet connection
    this.checkInternetConnection();
  }

  /**
   * This function is used to pass user info to DB
   * @param userInfo - User Info
   */
  login(userInfo): void {
    this.coreService.login(userInfo).subscribe(
      successUserInfo => {
        sessionStorage.setItem('userInfo', JSON.stringify(successUserInfo));
        this.coreService.closeLoginPopup();
      }
    );
  }

  /**
   * This function is used to check whether internet connetion is enabled or not.
   * If not it will show proper message to UI
   */
  checkInternetConnection(): void {
    this.connectionService.monitor().subscribe(isConnected => {
      this.isConnected = isConnected;
      const htmlContent = document.getElementsByTagName('html');
      if (this.isConnected) {
        this.toastr.successToastr('Welcome You are online', null, {
          position: 'top-center',
          enableHTML: true
        });
        htmlContent[0].style.pointerEvents = '';
        htmlContent[0].style.cursor = '';
      } else {
        this.toastr.errorToastr(
          'Please check your internet connection and try again...',
          null,
          {
            position: 'top-center',
            enableHTML: true
          }
        );
        htmlContent[0].style.pointerEvents = 'none';
        htmlContent[0].style.cursor = 'not-allowed';
      }
    });
  }
}

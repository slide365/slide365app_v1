import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
/**
 * @description
 * `DataService` is the bridge between API and front end calls
 */
export class DataService {
  constants: any;
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  constructor(private http: HttpClient) {
    this.constants = environment;
  }

  /**
   * This function is used to get info from API with `GET` call
   * @param methodName - Name of the method
   */
  getData(methodName): Observable<any> {
    const Url = this.constants.apiEndpoint + methodName;
    return this.http.get(Url).pipe(
      map(res => {
        return res;
      })
    );
  }

  /**
   * This function is used to pass data to API with `POST` call
   * @param methodName - Name Of the Method
   * @param data - Data
   */
  postData(methodName, data): Observable<any> {
    const body = JSON.stringify(data);
    const Url = this.constants.apiEndpoint + methodName;
    return this.http.post(Url, body, this.httpOptions).pipe(map(res => res));
  }

  /**
   * This function is used to post data with `POST` call but with `Session Id`
   * @param methodName - Name of the method
   * @param data - Data
   * @param SessionID - Session Id for `Authorization`
   */
  postElement(methodName, data, SessionID): Observable<any> {
    const body = JSON.stringify(data);
    const Url = this.constants.apiEndpoint + methodName;
    this.httpOptions.headers.append('Authorization', `${SessionID}`);
    return this.http.post(Url, body, this.httpOptions).pipe(map(res => res));
  }

  /**
   * This function is used to get info fromDb with `GET` call but with `Session Id`
   * @param methodNamee - Name of the method
   * @param SessionID - Session Id for `Authorization`
   */
  getAuthorizedDetails(methodName, SessionID) {
    const Url = this.constants.apiEndpoint + methodName;
    return this.http.get(Url, {
      headers: new HttpHeaders().append('Authorization', `${SessionID}`)
    });
  }

}

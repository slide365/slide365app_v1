import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { map } from 'rxjs/internal/operators/map';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { UserInfo } from 'src/app/_shared/models/user-info';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
/**
 * @description
 * Provides the way to keep all common API calls here
 */
export class CoreService {
  showSideBar = false;
  TotalItem = 0;
  sessionData = {
    SessionId: sessionStorage.getItem('UserID')
  };
  getCartItems = new BehaviorSubject(null);
  microsoftLogin = new BehaviorSubject(null);
  closeLoginModal = new BehaviorSubject(null);

  constructor(private dataService: DataService) {}

  /**
   * `Not in Use currently`
   * This function is used to triggered microsoft login
   */
  msLogin() {
    this.microsoftLogin.next(1);
  }

  /**
   * This function is used to close login popup once response is back from API
   */
  closeLoginPopup() {
    this.closeLoginModal.next(1);
  }

  /**
   * This is used to take all templates
   */
  getAllTemplates() {
    return this.dataService
      .getData('api/Category/GetCategoriesofTemplate')
      .pipe(map(res => res));
  }

  /**
   * This is used to take all domains
   */
  getAllDomains() {
    return this.dataService
      .getData('api/Category/GetDomain')
      .pipe(map(res => res));
  }

  /**
   * This is ued to take all designes
   */
  getAllDesign() {
    return this.dataService
      .getData('api/Category/GetDesign')
      .pipe(map(res => res));
  }

  /**
   * This is used to take all icons
   */
  getAllIcons() {
    return this.dataService
      .getData('api/Category/GetIcons')
      .pipe(map(res => res));
  }

  /**
   * This is used to take all slides with Category Id
   * @param CategoryID - Category Id
   */
  GetSubCategoryListByCategoryUID(CategoryID: string) {
    return this.dataService
      .getData(
        'api/Category/GetSubCategoryListByCategoryUID?CategoryID=' + CategoryID
      )
      .pipe(map(res => res));
  }

  /**
   * This is used to add item to an cart
   * @param ObjectID - Id
   */
  AddtoCart(ObjectID: string) {
    const SessionID = sessionStorage.getItem('sessionId');
    return this.dataService
      .getAuthorizedDetails(
        'api/Cart/AddToCart?ObjectID=' + ObjectID,
        SessionID
      )
      .pipe(map(res => res));
  }

  /**
   * This is used to search template/domain/design
   * @param Searchparam - Search parameter
   * @param Pageno - Page number
   * @param PageSize - Page size
   */
  GeSearchObjectList(Searchparam: string, Pageno: number, PageSize: number) {
    return this.dataService
      .getData(
        'api/Category/GetSearchObjectList?Searchparam=' +
          Searchparam +
          '&Pageno=' +
          Pageno +
          '&PageSize=' +
          PageSize
      )
      .pipe(map(res => res));
  }

  /**
   * This is used to get count of items in the cart
   */
  ShowtoCart() {
    const SessionID = sessionStorage.getItem('sessionId');
    return this.dataService
      .getAuthorizedDetails('api/Cart/ShowCart', SessionID)
      .pipe(map(res => res));
  }

  /**
   * This function is used to take object list
   * @param DomainID - Domain Id
   * @param SubCategoryID - SubCategory ID
   * @param Pageno - Page No
   * @param PageSize - Page Size
   */
  GetObjectList(
    DomainID: string,
    SubCategoryID: string,
    Pageno: number,
    PageSize: number
  ) {
    return this.dataService
      .getData(
        'api/Category/GetObjectList?DomainId=' +
          DomainID +
          '&SubCategoryId=' +
          SubCategoryID +
          '&Pageno=' +
          Pageno +
          '&PageSize=' +
          PageSize
      )
      .pipe(map(res => res));
  }

  /**
   * This function is used to send login info to db.
   * This function returns that info as success
   * @param userInfo - UserInfo
   * @returns UserInfo
   */
  login(userInfo: UserInfo): Observable<any> {
    const sessionId = sessionStorage.getItem('sessionId');
    return this.dataService
      .postElement('api/Login/Login', userInfo, sessionId)
      .pipe(map(res => res));
  }

  /**
   * This is used to take ordered history from DB
   * @param DateFrom - Date From
   * @param DateTo - Date To
   * @param PageNo - Page No
   * @param PageSize - Page Size
   */
  GetOrderHistory(DateFrom, DateTo, PageNo, PageSize) {
    const SessionID = sessionStorage.getItem('sessionId');
    return this.dataService
      .getAuthorizedDetails(
        'api/Cart/OrderHistory?DateFrom=' +
          DateFrom +
          '&DateTo=' +
          DateTo +
          '&PageNo=' +
          PageNo +
          '&PageSize=' +
          PageSize,
        SessionID
      )
      .pipe(map(res => res));
  }

  /**
   * This is used to call add to cart method with triggerring
   */
  callAddToCartService() {
    this.getCartItems.next(1);
  }

  /**
   * This function is used to remove items from cart
   * @param ObjectID - Object Id
   */
  RemoveFromCart(ObjectID: string) {
    const SessionID = sessionStorage.getItem('sessionId');
    return this.dataService
      .getAuthorizedDetails(
        'api/Cart/RemoveFromCart?ObjectID=' + ObjectID,
        SessionID
      )
      .pipe(map(res => res));
  }

  /**
   * This is used to pass token to API
   * @param token - Token
   */
  GetPayment(token: string) {
    const SessionID = sessionStorage.getItem('sessionId');
    return this.dataService
      .getAuthorizedDetails(
        'api/Payment/ProcessPayment?token=' + token,
        SessionID
      )
      .pipe(map(res => res));
  }

  /**
   * This is used to pass contact details to API
   * @param Name - Name
   * @param Email - Email
   * @param Subject - Subject
   * @param Message - Message
   */
  ContactUS(Name: string, Email: string, Subject: string, Message: string) {
    return this.dataService
      .getData(
        'api/Cart/ContactUS?Name=' +
          Name +
          '&Email=' +
          Email +
          '&Subject=' +
          Subject +
          '&Message=' +
          Message
      )
      .pipe(map(res => res));
  }

  /**
   * This is used to report a problem
   * @param Problem - Problem Name
   * @param Comments - Comment
   */
  ReportProblem(Problem: string, Comments: string) {
    return this.dataService
      .getData(
        'api/Cart/ReportProblem?Problem=' + Problem + '&Comments=' + Comments
      )
      .pipe(map(res => res));
  }

  /**
   * Go to back page
   */
  back() {
    window.history.back();
  }

  /**
   * This is used to download slide to PowerPoint
   * @param id - Id of slide
   */
  DownloadAndOpenPowerPoint(id: any) {
    return this.dataService
      .getData('api/Category/DownloadAndOpenPowerPoint?OrderId=' + id)
      .pipe(map(res => res));
  }

  /**
   * This function is used to create session id
   */
  createSessionId(): string {
    const s = [];
    const hexDigits = '0123456789abcdef';
    for (let i = 0; i < 36; i++) {
      s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = '4';
    // tslint:disable-next-line:no-bitwise
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
    s[8] = s[13] = s[18] = s[23] = '-';
    const userId = s.join('');
    sessionStorage.setItem('sessionId', userId);
    return userId;
  }

  /**
   * Google Login
   * @param accesstoken - Access Token
   * @param expiresin - Expires In
   */
  GoogleLogin(accesstoken: string, expiresin: string) {
    this.createSessionId();
    const SessionID = sessionStorage.getItem('sessionId');
    return this.dataService
      .getAuthorizedDetails(
        'api/GoogleLogin/GoogleGetUser?accestoken=' +
          accesstoken +
          '&expires_in=' +
          expiresin,
        SessionID
      )
      .pipe(map(res => res));
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataService } from './services/data.service';
import { CoreService } from './services/core.service';

@NgModule({
  declarations: [],
  imports: [CommonModule],
  providers: [DataService, CoreService]
})
export class CoreModule {}

import { Component, OnInit } from '@angular/core';
import { ShowCart } from 'src/app/_shared/models/ShowCart';
import { Router } from '@angular/router';
import { Domains } from 'src/app/_shared/interfaces/domains';
import { Category } from 'src/app/_shared/interfaces/category';
import { SubCategory } from 'src/app/_shared/interfaces/sub-category';
import { Menu } from 'src/app/_shared/models/menu';
import { CoreService } from '../services/core.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { MenuList } from 'src/app/_shared/models/menu-list';
@Component({
  selector: 'app-master-layout',
  templateUrl: './master-layout.component.html',
  styleUrls: ['./master-layout.component.scss']
})
export class MasterLayoutComponent implements OnInit {
  domains: Domains[];
  designs = [];
  icons = [];
  category: Category[];
  subcategory: SubCategory[];
  MenuItems: Menu;
  Domain: string;
  showcart: ShowCart[];
  public IsCart;
  public IsSetting;
  public IsUserlogin;
  public SearchTerm;
  public SelectedType = 'Template';
  public IsHeaderShow = true;
  bounce = false;
  showModel = false;
  showCartModel = false;
  showSettings = false;
  isLoggedInUser = false;
  listOfMenus = new MenuList();
  showSettingDropDown = false;
  constructor(
    public coreService: CoreService,
    public toastr: ToastrManager,
    public router: Router
  ) {
    this.coreService.getCartItems.subscribe((value: any) =>
      value ? this.GetCart(1) : null
    );
    const uuid = sessionStorage.getItem('User');
    this.isLoggedInUser = uuid == null ? false : true;
    this.isLoggedInUser =
      sessionStorage.getItem('userInfo') == null ? false : true;
  }

  ngOnInit() {
    this.IsSetting = false;
    this.GetCart(0);
    this.MenuListing();
  }

  showSearchBar() {
    this.IsHeaderShow = !this.IsHeaderShow;
  }

  hideSearchBar() {
    let classObj = document.getElementById('searchBarDiv');
    classObj.classList.remove('fadeInLeft');
    classObj.classList.add('fadeOutLeft');

    setTimeout(() => {
      this.IsHeaderShow = !this.IsHeaderShow;
      classObj = document.getElementById('searchBarDiv');
      if (classObj) {
        classObj.classList.remove('fadeOutLeft');
        classObj.classList.add('fadeInLeft');
      }
      this.router.navigate(['']);
    }, 1000);
  }

  Carts(): void {
    if (sessionStorage.getItem('userInfo') !== null) {
      // if (sessionStorage.getItem('User') !== null) {
      // this.showCartModel = !this.showCartModel;
      this.router.navigate(['cart/view-cart']);
      this.IsSetting = false;
    } else {
      this.toastr.warningToastr('Please login to continue...', 'Alert!', {
        position: 'top-center',
        enableHTML: true
      });
    }
  }

  Setting(): void {
    this.IsCart = false;
    this.IsSetting = true;
    this.showSettings = !this.showSettings;
  }

  User(): void {
    this.IsCart = false;
    this.IsSetting = false;
    if (sessionStorage.getItem('userInfo')) {
      this.showModel = !this.showModel; // Old Login
    } else {
      this.router.navigate(['auth/sign-up']);
    }
  }

  GetCart(flag): void {
    // const userid = sessionStorage.getItem('User');
    const userid = sessionStorage.getItem('userInfo');
    if (userid != null) {
      this.coreService.ShowtoCart().subscribe((_showcart: any) => {
        this.showcart = _showcart;
        if (this.showcart != null) {
          this.coreService.TotalItem = this.showcart.length;
          if (flag) {
            this.bounce = true;
            setTimeout(() => {
              this.bounce = false;
            }, 2000);
          }
        } else {
          this.coreService.TotalItem = 0;
        }
      });
    } else {
      this.coreService.TotalItem = 0;
    }
  }

  Search(): void {
    const SearchTerm = this.SearchTerm + '_' + this.SelectedType;
    this.router.navigate(['search/result/' + SearchTerm]);
  }

  MenuListing(): void {
    this.Domain = null;
    this.MenuItems = {
      Name: 'Main Menu',
      Type: 'Main',
      MenuList: [
        {
          Id: '1',
          Name: 'Template'
        },
        {
          Id: '2',
          Name: 'Domain'
        },
        {
          Id: '3',
          Name: 'Design'
        },
        {
          Id: '4',
          Name: 'Icons'
        }
      ]
    };
  }

  Menu(Id: string, Name: string): void {
    if (this.MenuItems.Type === 'Main') {
      if (Id === '1') {
        this.GetCategory('Template');
      }
      if (Id === '2') {
        this.GetDomain();
      }
      if (Id === '3') {
        this.GetDesign();
      }
      if (Id === '4') {
        this.GetDesign();
      }
      // if (Id === '4') {
      //   this.router.navigate(['/icon/view-all']);
      //   this.coreService.showSideBar = false;
      // }
    } else if (this.MenuItems.Type === 'Domain') {
      sessionStorage.setItem('DomainID', Id);
      this.Domain = Name;
      this.router.navigate(['domain/specific-domain/' + Id]);
      this.coreService.showSideBar = false;
    } else if (this.MenuItems.Type === 'Category') {
      this.GetSubCategory(Name, Id);
    } else if (this.MenuItems.Type === 'SubCategory') {
      sessionStorage.setItem('SubCategoryID', Id);
      if (this.Domain != null) {
        this.router.navigate(['domain/specific-domain/' + Id]);
        this.coreService.showSideBar = false;
      } else {
        this.router.navigate(['template/specific-template/' + Id]);
        this.coreService.showSideBar = false;
      }
    }
  }

  GetDomain(): void {
    this.coreService.getAllDomains().subscribe(res => {
      this.domains = res;
      this.MenuItems = {
        Name: 'Domain',
        MenuList: [],
        Type: 'Domain'
      };
      for (let index = 0; index < res.length; index++) {
        this.MenuItems.MenuList.push({
          Id: res[index].DomainId,
          Name: res[index].DomainName
        });
      }
    });
  }

  GetDesign(): void {
    this.coreService.getAllDesign().subscribe(res => {
      this.designs = res;
      this.MenuItems = {
        Name: 'Design',
        MenuList: [],
        Type: 'Design'
      };
      for (let index = 0; index < res.length; index++) {
        this.MenuItems.MenuList.push({
          Id: res[index].DesignUid,
          Name: res[index].DesignName
        });
      }
    });
  }

  GetIcons(): void {
    this.coreService.getAllIcons().subscribe(res => {
      this.icons = res;
      this.MenuItems = {
        Name: 'Icon',
        MenuList: [],
        Type: 'Icon'
      };
      for (let index = 0; index < res.length; index++) {
        this.MenuItems.MenuList.push({
          Id: res[index].IconUid,
          Name: res[index].IconName
        });
      }
    });
  }

  GetCategory(Name: string): void {
    this.coreService.getAllTemplates().subscribe(res => {
      this.category = res;
      this.MenuItems = {
        Name: Name,
        MenuList: [],
        Type: 'Category'
      };
      for (let index = 0; index < res.length; index++) {
        this.MenuItems.MenuList.push({
          Id: res[index].CategoryId,
          Name: res[index].CategoryName
        });
      }
    });
  }

  GetSubCategory(Name: string, CategoryID: string): void {
    this.coreService
      .GetSubCategoryListByCategoryUID(CategoryID)
      .subscribe(res => {
        this.subcategory = res;
        this.MenuItems = {
          Name: Name,
          MenuList: [],
          Type: 'SubCategory'
        };

        for (let index = 0; index < res.length; index++) {
          this.MenuItems.MenuList.push({
            Id: res[index].SubCategoryUId,
            Name: res[index].SubCategoryName
          });
        }
      });
  }

  Back(): void {
    if (this.MenuItems.Type === 'SubCategory') {
      if (this.Domain == null) {
        this.GetCategory('Template');
      } else {
        this.GetCategory(this.Domain);
      }
    } else if (this.MenuItems.Type === 'Category') {
      if (this.Domain == null) {
        this.MenuListing();
      } else {
        this.GetDomain();
      }
    } else if (this.MenuItems.Type === 'Domain') {
      this.MenuListing();
    }
  }

  callFromChildComponent(evt) {
    const keyname = Object.keys(evt);
    switch (keyname[0]) {
      case 'isLoggedInUser': {
        this.isLoggedInUser = evt[keyname[0]];
        break;
      }
    }
  }

  mouseEnterOnSettingIcon() {
    const ele = document.getElementById('setting-list-items');
    if (ele) {
      ele.classList.remove('d-none');
    }
  }

  mouseLeaveOnSettingIcon() {
    const ele = document.getElementById('setting-list-items');
    if (ele) {
      ele.classList.add('d-none');
    }
  }

  goTosubscriptionPage() {
    this.router.navigate(['subscription/plans']);
  }
}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-plans',
  templateUrl: './plans.component.html',
  styleUrls: ['./plans.component.scss']
})
export class PlansComponent implements OnInit {
  constructor() {}

  ngOnInit() {}

  onClickOfFree() {
    console.log('Free Clicked');
  }
  onClickOfWeekly() {
    console.log('Weekly Clicked');
  }
  onClickOfMonthly() {
    console.log('Monthly Clicked');
  }
}

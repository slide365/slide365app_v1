import { Component, OnInit } from '@angular/core';
import { Login } from '../login';
import { AuthService } from '../auth.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
/**
 * @description
 * Provides the way to create new User.
 */
export class SignUpComponent implements OnInit {
  model = new Login();
  errorMessage = '';
  isDone = false;
  isEmailValidationFlag = true;
  isOTPValidationFlag = true;
  constructor(
    private authService: AuthService,
    public toastr: ToastrManager
  ) {}

  ngOnInit() {}

  /**
   * This function get's called of on click of `Create account`
   * @param form - Form
   */
  onClickOfCreateAccount(form: any): void {
    if (form.valid) {
      this.authService.UserRegistration(this.model).subscribe(
        flag => {
          if (flag) {
            this.successToaster('Thank you for successfully registration.');
          } else {
            this.errorToaster(
              'There are some problem to create user. please try again later'
            );
          }
        },
        res => {}
      );
    }
  }

  /**
   * This function is used to validate if email id is exist or not
   */
  IsEmailExisits(): void {
    this.model.otp = '';
    if (this.model.emailId && this.model.emailId != '') {
      const isValide = this.validEmail(this.model.emailId);
      if (isValide) {
        this.isEmailValidationFlag = true;
        this.authService.IsEmailExisits(this.model).subscribe(
          flag => {
            if (flag) {
              this.isEmailValidationFlag = false;
              this.errorMessage =
                'This email already used. please try with another email id.';
            } else {
              this.sendOTP();
            }
          }
        );
      } else {
        this.isEmailValidationFlag = false;
        this.errorMessage = 'Invalid email id';
      }
    } else {
    }
  }

  /**
   * This function is used to send OTP to user
   */
  sendOTP(): void {
    this.authService.SendOTPToEmail(this.model).subscribe(
      flag => {
        if (flag) {
          this.successToaster('We are successfully send OTP on your email');
        } else {
          this.errorToaster(
            'There are some problem to send OPT. please try again later'
          );
        }
      },
      res => {}
    );
  }

  /**
   * This function is used to show Success message to user
   * @param message - `{{string}}` Success Message
   */
  successToaster(message): void {
    this.toastr.successToastr(message, null, {
      position: 'top-center',
      enableHTML: true
    });
  }

  /**
   * This function is used to show Error message to user
   * @param error - `{{string}}` Error Message
   */
  errorToaster(error): void {
    this.toastr.errorToastr(error, null, {
      position: 'top-center',
      enableHTML: true
    });
  }

  /**
   * This function is used to validate if email id is valid or not
   * @param email - Email
   * @returns Boolean
   */
  validEmail(email): boolean {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  /**
   * This fuction is used to validate if OTP is valid or not
   */
  validateOTP(): void {
    this.isDone = false;
    this.isOTPValidationFlag = true;
    if (this.model.otp && this.model.otp != '') {
      this.authService.checkValidOTP(this.model).subscribe(
        flag => {
          if (flag) {
            this.isDone = true;
          } else {
            this.isDone = false;
            this.isOTPValidationFlag = false;
            this.errorMessage = 'Invalid OTP';
          }
        }
      );
    }
  }
}

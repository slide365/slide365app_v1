import { Component, OnInit } from '@angular/core';
import { Login } from '../login';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  model = new Login();
  constructor(private router: Router) {}

  ngOnInit() {}

  /**
   * This function get's called of on click of `Forgot Password`
   * @param form - Form
   */
  onClickOfForgotPassword(form: any): void {
    if (form.valid) {
      this.router.navigate(['auth/reset-password']);
    } else {
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { Login } from '../login';
import { Router } from '@angular/router';
import { UserInfo } from '../../_shared/models/user-info';
import { AuthService } from '../auth.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
/**
 * @description
 * Provides the way to `Login`.
 */
export class SignInComponent implements OnInit {
  model = new Login();
  userInfo = new UserInfo();
  public IsLogin;
  constructor(
    private router: Router,
    private authService: AuthService,
    public toastr: ToastrManager,
    ) {}

  ngOnInit() {}

  /**
   * This function get's called of on click of `Login`
   * @param form - Form
   */
  onClickOfLogin(form: any): void {
    if (form.valid) {
      this.authService.IsLogin(this.model).subscribe(
        successUserInfo => {
          if (successUserInfo.length > 0) {
            sessionStorage.setItem(
              'userInfo',
              JSON.stringify(successUserInfo[0])
            );
            sessionStorage.setItem('sessionId', successUserInfo[0].UserUId);
            this.IsLogin = true;
            this.router.navigate(['']);
          } else {
            sessionStorage.removeItem('userInfo');
            this.toastr.warningToastr('Invalid Login details', 'Alert!', {
              position: 'top-center',
              enableHTML: true
            });
          }
        }
      );
    }
  }
}

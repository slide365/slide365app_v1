import { Injectable } from '@angular/core';
import { DataService } from '../_core/services/data.service';
import { map } from 'rxjs/internal/operators/map';
import { Login } from './login';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  model = new Login();
  constructor(private dataService: DataService) {}

  IsEmailExisits(model: Login) {
    return this.dataService
      .postData('api/Login/IsEmailExists', model)
      .pipe(map(res => res));
  }

  SendOTPToEmail(model: Login) {
    return this.dataService
      .postData('api/Login/SendOTPToEmail', model)
      .pipe(map(res => res));
  }

  checkValidOTP(model: Login) {
    return this.dataService
      .postData('api/Login/checkValidOTP', model)
      .pipe(map(res => res));
  }

  UserRegistration(model: Login) {
    return this.dataService
      .postData('api/Login/UserRegistration', model)
      .pipe(map(res => res));
  }

  IsLogin(model: Login) {
    return this.dataService
      .postData('api/Login/Login', model)
      .pipe(map(res => res));
  }
}

import { Component, OnInit } from '@angular/core';
import { Login } from '../login';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  model = new Login();
  constructor(private router: Router) {}

  ngOnInit() {}

  /**
   * This function get's called of on click of `Reset Password`
   * @param form - Form
   */
  onClickOfResetPassword(form: any): void {
    if (form.valid) {
      this.router.navigate(['auth/sign-in']);
    } else {
    }
  }
}

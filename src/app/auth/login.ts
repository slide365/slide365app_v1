export class Login {
  firstName: string;
  lastName: string;
  emailId: string;
  password: string;
  confirmPassword: string;
  otp: string;

  /**
   * This model is used for `login` and `sign up`
   */
  constructor() {
    this.firstName = null;
    this.lastName = null;
    this.emailId = null;
    this.password = null;
    this.confirmPassword = null;
    this.otp = null;
  }
}

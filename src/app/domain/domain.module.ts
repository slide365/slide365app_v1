import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DomainRoutingModule } from './domain-routing.module';
import { ViewAllDomainComponent } from './view-all-domain/view-all-domain.component';
import { SpecificDomainsComponent } from './specific-domains/specific-domains.component';
import { DomainService } from './domain.service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { SharedModule } from '../_shared/_shared.module';

@NgModule({
  declarations: [ViewAllDomainComponent, SpecificDomainsComponent],
  imports: [CommonModule, DomainRoutingModule, NgxSpinnerModule, SharedModule],
  providers: [DomainService]
})
export class DomainModule {}

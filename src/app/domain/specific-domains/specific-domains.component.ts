import { Component, OnInit } from '@angular/core';
import { DomainService } from '../domain.service';
import { CoreService } from 'src/app/_core/services/core.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-specific-domains',
  templateUrl: './specific-domains.component.html',
  styleUrls: ['./specific-domains.component.scss']
})
export class SpecificDomainsComponent implements OnInit {
  IsAll = false;
  DomainName: string;
  ObjectListing: any;
  PageNo: number;
  PageSize: number;
  TotalPage: number;
  public IsSideBar;
  imageDetails = null;
  constructor(
    private domainService: DomainService,
    public coreService: CoreService,
    public toastr: ToastrManager
  ) {
    this.ObjectListing = {
      Count: 0,
      ObjectArray: []
    };
  }

  ngOnInit() {
    this.IsAll = sessionStorage.getItem('isAll') === 'False' ? false : true;
    this.DomainName = sessionStorage.getItem('DomainName');
    this.PageNo = 1;
    this.PageSize = 8;
    this.GetObject(this.PageNo);
  }

  SideBar(): void {
    this.IsSideBar = true;
  }

  GetObject(PageNo: number): void {
    const FileObjectUID = sessionStorage.getItem('DomainID');
    this.domainService
      .GetObjectList(FileObjectUID, PageNo, this.PageSize)
      .subscribe(_ObjectListing => {
        this.ObjectListing = {
          Count: _ObjectListing.Count,
          ObjectArray: _ObjectListing.ObjectArray
        };
        this.TotalPage = Math.ceil(this.ObjectListing.Count / this.PageSize);
      });
  }

  FirstPage(): void {
    if (this.PageNo !== 1) {
      this.PageNo = 1;
      this.GetObject(1);
    }
  }

  NextPage(): void {
    if (this.PageNo !== this.TotalPage) {
      this.PageNo++;
      this.GetObject(this.PageNo);
    }
  }

  PreviousPage(): void {
    if (this.PageNo !== 1) {
      this.PageNo--;
      this.GetObject(this.PageNo);
    }
  }

  LastPage(): void {
    if (this.PageNo !== this.TotalPage) {
      this.PageNo = this.TotalPage;
      this.GetObject(this.TotalPage);
    }
  }

  AddtoCart(ObjectID: string): void {
    // const userid = sessionStorage.getItem('User');
    const userid = sessionStorage.getItem('userInfo');

    if (userid != null) {
      this.coreService.AddtoCart(ObjectID).subscribe(
        () => {
          this.coreService.callAddToCartService();
        },
        error =>
          this.toastr.errorToastr(error.error.Message, null, {
            position: 'top-center',
            enableHTML: true
          })
      );
    } else {
      this.toastr.warningToastr('Please login to continue...', null, {
        position: 'top-center',
        enableHTML: true
      });
    }
  }

  openPopup(msg: string): void {}

  insertSlide(id) {
    const text = (<any>window).external.InsertIcon(id);
  }
}

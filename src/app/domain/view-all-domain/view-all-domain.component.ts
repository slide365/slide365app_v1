import { Component, OnInit } from '@angular/core';
import { CoreService } from 'src/app/_core/services/core.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-view-all-domain',
  templateUrl: './view-all-domain.component.html',
  styleUrls: ['./view-all-domain.component.scss']
})
export class ViewAllDomainComponent implements OnInit {
  allDomains = [];
  constructor(
    private spinner: NgxSpinnerService,
    public coreService: CoreService,
    private router: Router
  ) {}

  ngOnInit() {
    this.spinner.show();
    this.coreService.getAllDomains().subscribe(res => {
      this.spinner.hide();
      this.allDomains = res;
      setTimeout(() => {
        if (sessionStorage.backId) {
          const id = document.querySelector('#domain-' + sessionStorage.backId);
          // tslint:disable-next-line:no-unused-expression
          id ? id.scrollIntoView() : null;
        }
      }, 500);
    });
  }

  HomeRedirect(): void {
    this.router.navigate(['']);
  }

  GetDomain(): void {}

  GoToCategory(DomainID: string, DomainName: string): void {
    sessionStorage.setItem('isAll', 'True');
    sessionStorage.setItem('DomainName', DomainName);
    sessionStorage.setItem('DomainID', DomainID);
    sessionStorage.setItem('backId', DomainID);
    this.router.navigate(['domain/specific-domain/' + DomainID]);
  }
}

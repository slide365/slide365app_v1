import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewAllDomainComponent } from './view-all-domain/view-all-domain.component';
import { SpecificDomainsComponent } from './specific-domains/specific-domains.component';

const routes: Routes = [
  {
    path: 'view-all',
    component: ViewAllDomainComponent,
    data: {
      title: 'view-all'
    }
  },
  {
    path: 'specific-domain/:Id',
    component: SpecificDomainsComponent,
    data: {
      title: 'specific-domains'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DomainRoutingModule { }

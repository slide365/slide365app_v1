import { Component, OnInit } from '@angular/core';
import { CoreService } from 'src/app/_core/services/core.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-view-all-template',
  templateUrl: './view-all-template.component.html',
  styleUrls: ['./view-all-template.component.scss']
})
export class ViewAllTemplateComponent implements OnInit {
  allTemplates = [];

  constructor(
    private spinner: NgxSpinnerService,
    public coreService: CoreService,
    private router: Router
  ) {}

  ngOnInit() {
    this.spinner.show();
    this.coreService.getAllTemplates().subscribe(res => {
      this.spinner.hide();
      this.allTemplates = res;
    });
  }

  HomeRedirect(): void {
    this.router.navigate(['/home']);
  }

  GotoSubCategory(CategoryID: string): void {
    sessionStorage.setItem('CategoryID', CategoryID);
    this.router.navigate(['template/view-all-specific-template/' + CategoryID]);
  }

  back() {
    window.history.back();
  }
}

import { Component, OnInit } from '@angular/core';
import { TemplateService } from '../template.service';
import { Router } from '@angular/router';
import { SubCategory } from 'src/app/_shared/interfaces/sub-category';
import { CoreService } from 'src/app/_core/services/core.service';

@Component({
  selector: 'app-view-all-specific-templates',
  templateUrl: './view-all-specific-templates.component.html',
  styleUrls: ['./view-all-specific-templates.component.scss']
})
export class ViewAllSpecificTemplatesComponent implements OnInit {
  IsAll = false;
  SubCategory: SubCategory[];
  TemplbadgeName1: string;
  public IsSideBar;
  constructor(
    private router: Router,
    public coreService: CoreService,
    private templateService: TemplateService
  ) {}

  ngOnInit() {
    this.IsAll = sessionStorage.getItem('isAll') === 'False' ? false : true;
    const CategoryID = sessionStorage.getItem('CategoryID');
    this.GetSubCategory(CategoryID);
  }

  HomeRedirect(): void {
    this.router.navigate(['Home']);
  }

  GetSubCategory(CategoryID: string): void {
    this.templateService
      .GetAllSubCategoryWithImagesByCategoryID(CategoryID)
      .subscribe(res => {
        this.SubCategory = res;
        if (this.SubCategory.length > 0) {
          this.TemplbadgeName1 = this.SubCategory[0].CategoryName;
        }
      });
  }

  GotoObjectlist(SubCategoryID: string): void {
    sessionStorage.setItem('TemplbadgeName1', this.TemplbadgeName1);
    sessionStorage.setItem('SubCategoryID', SubCategoryID);
    this.router.navigate(['template/specific-template/' + SubCategoryID]);
  }
}

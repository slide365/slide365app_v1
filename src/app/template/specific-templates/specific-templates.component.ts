import { Component, OnInit } from '@angular/core';
import { ObjectListing } from 'src/app/_shared/interfaces/object-listing';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { CoreService } from 'src/app/_core/services/core.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { TemplateService } from '../template.service';
@Component({
  selector: 'app-specific-templates',
  templateUrl: './specific-templates.component.html',
  styleUrls: ['./specific-templates.component.scss']
})
export class SpecificTemplatesComponent implements OnInit {
  IsAll = false;
  CategoryID: string;
  TemplbadgeName1: string;
  TemplbadgeName2: string;
  ObjectListing: ObjectListing;
  PageNo: number;
  PageSize: number;
  TotalPage: number;
  public IsSideBar;
  imageDetails: any;
  constructor(
    public coreService: CoreService,
    private router: Router,
    private templateService: TemplateService,
    private spinner: NgxSpinnerService,
    public toastr: ToastrManager
  ) {
    this.ObjectListing = {
      Count: 0,
      ObjectArray: []
    };
  }

  ngOnInit() {
    this.IsAll = sessionStorage.getItem('isAll') === 'False' ? false : true;
    this.TemplbadgeName1 = sessionStorage.getItem('TemplbadgeName1');
    this.CategoryID = sessionStorage.getItem('CategoryID');
    this.PageNo = 1;
    this.PageSize = 8;
    this.GetObject(this.PageNo);
  }

  GotoSubCategory(): void {
    sessionStorage.setItem('CategoryID', this.CategoryID);
    this.router.navigate([
      'template/view-all-specific-template/' + this.CategoryID
    ]);
  }

  SideBar(): void {
    this.IsSideBar = true;
  }

  GetObject(PageNo: number): void {
    const DomainID = null;
    const SubCategoryID = sessionStorage.getItem('SubCategoryID');
    this.spinner.show();
    this.templateService
      .GetObjectList(SubCategoryID, PageNo, this.PageSize)
      .subscribe(
        _ObjectListing => {
          this.spinner.hide();
          this.ObjectListing = {
            Count: _ObjectListing.Count,
            ObjectArray: _ObjectListing.ObjectArray
          };
          if (this.ObjectListing.ObjectArray.length > 0) {
            this.TemplbadgeName2 = this.ObjectListing.ObjectArray[0].ObjectName;
          }
          this.TotalPage = Math.ceil(this.ObjectListing.Count / this.PageSize);
        },
        () => this.spinner.hide()
      );
  }

  FirstPage(): void {
    if (this.PageNo !== 1) {
      this.PageNo = 1;
      this.GetObject(1);
    }
  }

  NextPage(): void {
    if (this.PageNo !== this.TotalPage) {
      this.PageNo++;
      this.GetObject(this.PageNo);
    }
  }

  PreviousPage(): void {
    if (this.PageNo !== 1) {
      this.PageNo--;
      this.GetObject(this.PageNo);
    }
  }

  LastPage(): void {
    if (this.PageNo !== this.TotalPage) {
      this.PageNo = this.TotalPage;
      this.GetObject(this.TotalPage);
    }
  }

  AddtoCart(ObjectID: string): void {
    // const userid = sessionStorage.getItem('User');
    const userid = sessionStorage.getItem('userInfo');

    if (userid != null) {
      this.spinner.show();
      this.coreService.AddtoCart(ObjectID).subscribe(
        cart => {
          this.coreService.callAddToCartService();

          this.spinner.hide();
        },
        error => {
          this.spinner.hide();
          this.toastr.errorToastr(error.error.Message, null, {
            position: 'top-center',
            enableHTML: true
          });
        }
      );
    } else {
      this.toastr.warningToastr('Please login to continue...', null, {
        position: 'top-center',
        enableHTML: true
      });
    }
  }

  openPopup(msg: string): void {}

  insertSlide(id) {
    const text = (<any>window).external.InsertIcon(id);
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewAllTemplateComponent } from './view-all-template/view-all-template.component';
import { SpecificTemplatesComponent } from './specific-templates/specific-templates.component';
import { ViewAllSpecificTemplatesComponent } from './view-all-specific-templates/view-all-specific-templates.component';

const routes: Routes = [
  {
    path: 'view-all',
    component: ViewAllTemplateComponent,
    data: {
      title: 'view-all'
    }
  },
  {
    path: 'view-all-specific-template/:Id',
    component: ViewAllSpecificTemplatesComponent,
    data: {
      title: 'specific-template'
    }
  },
  {
    path: 'specific-template/:Id',
    component: SpecificTemplatesComponent,
    data: {
      title: 'specific-template'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TemplateRoutingModule {}

import { Injectable } from '@angular/core';
import { DataService } from '../_core/services/data.service';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class TemplateService {
  constructor(private dataService: DataService) {}

  GetAllSubCategoryWithImagesByCategoryID(CategoryID: string) {
    return this.dataService
      .getData(
        'api/Category/GetAllSubCategoryWithImagesByCategoryID?CategoryID=' +
          CategoryID
      )
      .pipe(map(res => res));
  }

  GetObjectList(DomainID: string, Pageno: number, PageSize: number) {
    return this.dataService
      .getData(
        'api/Category/GetObjectList?FileObjectUID=' +
          DomainID +
          '&Pageno=' +
          Pageno +
          '&PageSize=' +
          PageSize
      )
      .pipe(map(res => res));
  }
}

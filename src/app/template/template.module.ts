import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TemplateRoutingModule } from './template-routing.module';
import { ViewAllTemplateComponent } from './view-all-template/view-all-template.component';
import { SpecificTemplatesComponent } from './specific-templates/specific-templates.component';
import { TemplateService } from './template.service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ViewAllSpecificTemplatesComponent } from './view-all-specific-templates/view-all-specific-templates.component';
import { SharedModule } from '../_shared/_shared.module';

@NgModule({
  declarations: [
    ViewAllTemplateComponent,
    SpecificTemplatesComponent,
    ViewAllSpecificTemplatesComponent
  ],
  imports: [
    CommonModule,
    TemplateRoutingModule,
    NgxSpinnerModule,
    SharedModule
  ],
  providers: [TemplateService]
})
export class TemplateModule {}

import { Component, OnInit } from '@angular/core';
import { CoreService } from 'src/app/_core/services/core.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-view-all-design',
  templateUrl: './view-all-design.component.html',
  styleUrls: ['./view-all-design.component.scss']
})
export class ViewAllDesignComponent implements OnInit {
  allDesign = [];
  constructor(
    private spinner: NgxSpinnerService,
    public coreService: CoreService,
    private router: Router
  ) {}

  ngOnInit() {
    this.spinner.show();
    this.coreService.getAllDesign().subscribe(res => {
      this.spinner.hide();
      this.allDesign = res;
      setTimeout(() => {
        if (sessionStorage.backId) {
          const id = document.querySelector('#domain-' + sessionStorage.backId);
          // tslint:disable-next-line:no-unused-expression
          id ? id.scrollIntoView({ behavior: 'auto' }) : null;
        }
      }, 500);
    });
  }

  HomeRedirect(): void {
    this.router.navigate(['']);
  }

  GetDomain(): void {}

  GoToCategory(DesignUid: string, DesignName: string): void {
    sessionStorage.setItem('DesignName', DesignName);
    sessionStorage.setItem('DesignUid', DesignUid);
    sessionStorage.setItem('backId', DesignUid);
    this.router.navigate(['design/specific-design/' + DesignUid]);
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewAllDesignComponent } from './view-all-design/view-all-design.component';
import { SpecificDesignComponent } from './specific-design/specific-design.component';

const routes: Routes = [
  {
    path: 'view-all',
    component: ViewAllDesignComponent,
    data: {
      title: 'view-all'
    }
  },
  {
    path: 'specific-design/:Id',
    component: SpecificDesignComponent,
    data: {
      title: 'specific-design'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DesignRoutingModule {}

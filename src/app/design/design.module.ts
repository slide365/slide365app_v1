import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DesignRoutingModule } from './design-routing.module';
import { ViewAllDesignComponent } from './view-all-design/view-all-design.component';
import { SpecificDesignComponent } from './specific-design/specific-design.component';
import { DesignService } from './design.service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { SharedModule } from '../_shared/_shared.module';

@NgModule({
  declarations: [ViewAllDesignComponent, SpecificDesignComponent],
  imports: [CommonModule, DesignRoutingModule, NgxSpinnerModule, SharedModule],
  providers: [DesignService]
})
export class DesignModule {}

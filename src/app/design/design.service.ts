import { Injectable } from '@angular/core';
import { DataService } from '../_core/services/data.service';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class DesignService {
  constructor(private dataService: DataService) {}

  GetObjectList(DesignUid: string, Pageno: number, PageSize: number) {
    return this.dataService
      .getData(
        'api/Category/GetDesignByDesignUid?DesignUid=' +
          DesignUid +
          '&Pageno=' +
          Pageno +
          '&PageSize=' +
          PageSize
      )
      .pipe(map(res => res));
  }

  GetObjectList1(DomainID: string, Pageno: number, PageSize: number) {
    return this.dataService
      .getData(
        'api/Category/GetObjectList?FileObjectUID=' +
          DomainID +
          '&Pageno=' +
          Pageno +
          '&PageSize=' +
          PageSize
      )
      .pipe(map(res => res));
  }
}

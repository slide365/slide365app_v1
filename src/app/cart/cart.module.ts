import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxSpinnerModule } from 'ngx-spinner';
import { CartRoutingModule } from './cart-routing.module';
import { ViewCartComponent } from './view-cart/view-cart.component';

@NgModule({
  declarations: [ViewCartComponent],
  imports: [CommonModule, CartRoutingModule, NgxSpinnerModule]
})
export class CartModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewCartComponent } from './view-cart/view-cart.component';

const routes: Routes = [
  {
    path: 'view-cart',
    component: ViewCartComponent,
    data: {
      title: 'view-cart'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CartRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { CoreService } from 'src/app/_core/services/core.service';
import { ShowCart } from 'src/app/_shared/models/ShowCart';
import { analyzeAndValidateNgModules } from '@angular/compiler';

@Component({
  selector: 'app-view-cart',
  templateUrl: './view-cart.component.html',
  styleUrls: ['./view-cart.component.scss']
})
export class ViewCartComponent implements OnInit {
  showcart: ShowCart[];
  public CartAmount;
  public TotalItem;
  show = false;

  constructor(
    public coreService: CoreService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.TotalItem = 0;
    this.GetCart();
  }

  // ngOnChanges(changes: SimpleChanges) {
  //   if (
  //     changes
  //       ? changes.showCartModel
  //         ? changes.showCartModel.currentValue === true
  //         : false
  //       : false
  //   ) {
  //     this.show = !this.show;
  //     // tslint:disable-next-line:no-unused-expression
  //     this.show ? this.GetCart() : null;
  //   }
  // }

  // Close(flag = 0): void {
  //   let classObj = document.getElementById('cartUserModal');
  //   classObj.classList.remove('fadeInLeft');
  //   classObj.classList.add('fadeOutRight');

  //   setTimeout(() => {
  //     classObj = document.getElementById('cartUserModal');
  //     classObj.classList.remove('fadeOutRight');
  //     classObj.classList.add('fadeInLeft');
  //     this.show = false;
  //     if (flag) {
  //       this.ngOnInit();
  //     }
  //   }, 1000);
  // }

  GetCart(): void {
    // const userid = sessionStorage.getItem('User');
    const userid = sessionStorage.getItem('userInfo');
    if (userid != null) {
      this.spinner.show();
      this.coreService.ShowtoCart().subscribe(
        (_showcart: any) => {
          this.spinner.hide();
          this.showcart = _showcart;
          this.CartAmount = 0;

          if (this.showcart != null) {
            this.TotalItem = this.showcart.length;
            for (let i = 0; i < this.showcart.length; i++) {
              this.CartAmount += this.showcart[i].Price;
            }
          }
        },
        () => this.spinner.hide()
      );
    }
  }
  openPopup(msg: string): void {}

  Remove(ObjectID: string): void {
    this.spinner.show();
    this.coreService.RemoveFromCart(ObjectID).subscribe(
      () => {
        this.spinner.hide();
        // this.eventEmitter.emit();
        for (let i = 0; i < this.showcart.length; i++) {
          if (ObjectID === this.showcart[i].ObjectIid) {
            this.CartAmount -= this.showcart[i].Price;
            this.showcart.splice(i, 1);
            this.TotalItem = this.showcart.length;
          }
        }
      },
      error => {
        this.spinner.hide();
        setTimeout(() => this.openPopup(error.error.Message));
      }
    );
  }

  gotoPage() {
    // this.coreService.DownloadAndOpenPowerPoint().subscribe(response => {
    //   PowerPoint.createPresentation(response);
    // });
    // this.coreService
    //   .DownloadAndOpenPowerPoint()
    //   .subscribe(function res(response) {
    //     PowerPoint.createPresentation(response);
    //   });
    this.router.navigate(['payment']);
  }
}

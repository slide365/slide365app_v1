import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/_shared/interfaces/user';
import { ActivatedRoute } from '@angular/router';
import { AccountService } from '../account.service';

@Component({
  selector: 'app-gmail-success',
  template: ``,
  styleUrls: []
})
export class GmailSuccessComponent implements OnInit {
  public code;
  public state;
  public access_token;
  public Expires_in;
  public newwindow;
  public uuid;
  user: User;

  constructor(
    private route: ActivatedRoute,
    private accountService: AccountService
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.access_token = this.route.snapshot.fragment
        .split('&')[1]
        .split('=')[1];
      this.Expires_in = this.route.snapshot.fragment
        .split('&')[3]
        .split('=')[1];
      this.accountService
        .GoogleLogin(this.access_token, this.Expires_in)
        .subscribe((Users: any) => {
          this.user = Users;
          sessionStorage.setItem('User', JSON.stringify(this.user));
          // window.location.href = sessionStorage.getItem('LastPage');
        });
    });
  }

  CreateGuid(): void {
    const s = [];
    const hexDigits = '0123456789abcdef';
    for (let i = 0; i < 36; i++) {
      s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = '4'; // bits 12-15 of the time_hi_and_version field to 0010
    // tslint:disable-next-line:no-bitwise
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1); // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = '-';

    this.uuid = s.join('');
    sessionStorage.setItem('UserID', this.uuid);
  }
}

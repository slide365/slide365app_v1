import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountRoutingModule } from './account-routing.module';
import { LinkedinSuccessComponent } from './linkedin-success/linkedin-success.component';
import { GmailSuccessComponent } from './gmail-success/gmail-success.component';
import { MicrosoftSuccessComponent } from './microsoft-success/microsoft-success.component';
import { AccountService } from './account.service';
import { SharedModule } from '../_shared/_shared.module';
import { ContactUsComponent } from './setting-pages/contact-us/contact-us.component';
import { ReportProblemComponent } from './setting-pages/report-problem/report-problem.component';
import { TermsPolicyComponent } from './setting-pages/terms-policy/terms-policy.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    LinkedinSuccessComponent,
    GmailSuccessComponent,
    MicrosoftSuccessComponent,
    ContactUsComponent,
    ReportProblemComponent,
    TermsPolicyComponent
  ],
  imports: [CommonModule, AccountRoutingModule, SharedModule, FormsModule],
  providers: [AccountService]
})
export class AccountModule {}

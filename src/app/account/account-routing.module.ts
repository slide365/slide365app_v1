import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GmailSuccessComponent } from './gmail-success/gmail-success.component';
import { MicrosoftSuccessComponent } from './microsoft-success/microsoft-success.component';
import { LinkedinSuccessComponent } from './linkedin-success/linkedin-success.component';
import { ReportProblemComponent } from './setting-pages/report-problem/report-problem.component';
import { ContactUsComponent } from './setting-pages/contact-us/contact-us.component';
import { TermsPolicyComponent } from './setting-pages/terms-policy/terms-policy.component';

const routes: Routes = [
  {
    path: '',
    component: GmailSuccessComponent,
    data: {
      title: 'gmail-success'
    }
  },
  {
    path: 'microsoft-success',
    component: MicrosoftSuccessComponent,
    data: {
      title: 'microsoft-success'
    }
  },
  {
    path: 'linkedin-success',
    component: LinkedinSuccessComponent,
    data: {
      title: 'linkedin-success'
    }
  },
  {
    path: 'contact-us',
    component: ContactUsComponent,
    data: {
      title: 'contact-us'
    }
  },
  {
    path: 'report-problem',
    component: ReportProblemComponent,
    data: {
      title: 'report-problem'
    }
  },
  {
    path: 'terms-policy',
    component: TermsPolicyComponent,
    data: {
      title: 'terms-policy'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule {}

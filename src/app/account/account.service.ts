import { Injectable } from '@angular/core';
import { DataService } from '../_core/services/data.service';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  constructor(private dataService: DataService) {}

  GoogleLogin(accesstoken: string, expiresin: string) {
    const SessionID = sessionStorage.getItem('sessionId');

    return this.dataService
      .getAuthorizedDetails(
        'api/GoogleLogin/GoogleGetUser?accestoken=' +
          accesstoken +
          '&expires_in=' +
          expiresin,
        SessionID
      )
      .pipe(map(res => res));
  }
}

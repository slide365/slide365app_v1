import { Component, OnInit } from '@angular/core';
import { CoreService } from 'src/app/_core/services/core.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
  Name = null;
  Email = null;
  Subject = null;
  Message = null;

  constructor(private coreService: CoreService, public toastr: ToastrManager) {}

  ngOnInit() {}

  onClickOFSendContact(form) {
    form.submitted = true;
    if (form.valid) {
      this.coreService
        .ContactUS(this.Name, this.Email, this.Subject, this.Message)
        .subscribe(
          () => {
            this.toastr.successToastr(
              'Your message submitted successfully',
              null,
              {
                position: 'top-center',
                enableHTML: true
              }
            );
            this.onClickOFResetContact(form);
          },
          error =>
            this.toastr.errorToastr(error.error.Message, null, {
              position: 'top-center',
              enableHTML: true
            })
        );
    }
  }

  onClickOFResetContact(form) {
    form.submitted = false;
    form.resetForm();
    this.Name = null;
    this.Email = null;
    this.Subject = null;
    this.Message = null;
  }
}

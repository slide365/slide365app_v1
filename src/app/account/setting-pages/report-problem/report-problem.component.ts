import { Component, OnInit } from '@angular/core';
import { CoreService } from 'src/app/_core/services/core.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-report-problem',
  templateUrl: './report-problem.component.html',
  styleUrls: ['./report-problem.component.scss']
})
export class ReportProblemComponent implements OnInit {
  problem = null;
  comment = null;

  constructor(private coreService: CoreService, public toastr: ToastrManager) {}

  ngOnInit() {}

  onClickOFSendComment(form) {
    form.submitted = true;
    if (form.valid) {
      this.coreService.ReportProblem(this.problem, this.comment).subscribe(
        () => {
          this.toastr.successToastr(
            'Your message submitted successfully',
            null,
            {
              position: 'top-center',
              enableHTML: true
            }
          );
          this.onClickOFResetComment(form);
        },
        error =>
          this.toastr.errorToastr(error.error.Message, null, {
            position: 'top-center',
            enableHTML: true
          })
      );
    }
  }

  onClickOFResetComment(form) {
    form.resetForm();
    this.problem = null;
    this.comment = null;
  }
}

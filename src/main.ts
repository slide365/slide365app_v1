import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
// import * as OfficeHelpers from '@microsoft/office-js-helpers';
import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { Authenticator } from '@microsoft/office-js-helpers';
if (environment.production) {
  enableProdMode();
}

/**
 * un-comment this if you want to run application inside browser
 */
// platformBrowserDynamic()
//   .bootstrapModule(AppModule)
//   .catch(err => console.error(err));

/**
 * Keep this code if you want to run application inside powerpoint
 * It can be a online or offline
 */
Office.initialize = function() {
  if (Authenticator.isAuthDialog()) {
    return;
  }
  platformBrowserDynamic().bootstrapModule(AppModule);
};
